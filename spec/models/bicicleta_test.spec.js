var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function() {
    beforeAll(function(done) {
        var mongoDB = 'mongodb://localhost/red_bicicletas_testdb';
        mongoose.connect(mongoDB, { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true });
        
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.on('open', function() {
            console.log('We are connected to test database!');
            done();
        });
    });

    afterAll(function(done) {
        const db = mongoose.connection;

        db.on('close', function() {
            console.log('We are disconnected to test database!');
            done();
        });

        mongoose.disconnect();
    });
    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', (done) => {
            var bici = Bicicleta.createInstance(1, 'verde', 'urbana', [-34.5, -54.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe('verde');
            expect(bici.modelo).toBe('urbana');
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-54.1);

            done();
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacía', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('agrega solo una bici', (done) => {
            var aBici = new Bicicleta({ code: 1, color: 'rojo', modelo: 'urbana' });
            Bicicleta.add(aBici, function(err, newBici) {
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis) {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('debo devolver la bici con code 1', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({ code: 1, color: 'verde', modelo: 'urbana' });
                Bicicleta.add(aBici, function(err, newBici) {
                    if (err) console.log(err);
                    
                    var aBici2 = new Bicicleta({ code: 2, color: 'rojo', modelo: 'urbana' });
                    Bicicleta.add(aBici2, function(err, newBici) {
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function(err, targetBici) {
                            expect(targetBici.code).toEqual(aBici.code);
                            expect(targetBici.color).toEqual(aBici.color);
                            expect(targetBici.modelo).toEqual(aBici.modelo);

                            done();
                        });
                    });
                });


            });
        });
    });

    describe('Bicicleta.removeByCode', () => {
        it('debo eliminar la bici con code 1', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({ code: 1, color: 'verde', modelo: 'urbana' });
                Bicicleta.add(aBici, function(err, newBici) {
                    if (err) console.log(err);
                    
                    var aBici2 = new Bicicleta({ code: 2, color: 'rojo', modelo: 'urbana' });
                    Bicicleta.add(aBici2, function(err, newBici) {
                        if (err) console.log(err);
                        Bicicleta.removeByCode(1, function(err) {                            
                            Bicicleta.findByCode(1, function(err, targetBici) {
                                expect(targetBici).toEqual(null);
                                done();
                            });                           
                        });
                    });
                });


            });
        });
    });
});