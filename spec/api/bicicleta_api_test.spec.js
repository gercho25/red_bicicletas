var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');


describe('Bicileta API', () => {

    beforeEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            done();
        });
    });

    describe('GET BICICLETAS /', () => {
        it('Status 200', () => {
            var a = new Bicicleta(1, 'rojo', 'urbana', [-37.997447, -57.5437]);
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', function(error, response, body) {
                expect(response.statusCode).toBe(200);
            });
        });
    });

    // describe('POST BICICLETAS /create', () => {
    //     it('Status 200', (done) => {
    //         var headers = {'content-type': 'application/json'};
    //         var aBici = '{ "id": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54 }';

    //         request.post({
    //             headers: headers,
    //             url: 'http://localhost:3000/api/bicicletas/create',
    //             body: aBici
    //         }, function(error, response, body) {
    //             expect(response.statusCode).toBe(200);
    //             expect(Bicicleta.findById(10).color).toBe('rojo');
    //             done();
    //         });

    //     });
    // });

    // describe('DELETE BICICLETAS /delete', () => {
    //     it('Status 204', (done) => {
    //         var a = Bicicleta.createInstance(1, 'negro', 'urbano', [-34.60, -58.38]);
    //         Bicicleta.add(a, function(err, newBici) {
    //             var headers = {'content-type': 'application/json'};
    //             var aBici = '{ "id": 11 }';

    //             request.delete({
    //                 headers: headers,
    //                 url: 'http://localhost:3000/api/bicicletas/create',
    //                 body: aBici
    //             }, function(error, response, body) {
    //                 expect(response.statusCode).toBe(204);
    //                 expect(Bicicleta.findById(11).color).toBe(null);
    //                 done();
    //             });

    //         });

            
    //     });
    // });

});
