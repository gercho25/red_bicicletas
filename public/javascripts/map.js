var map = L.map('main_map').setView([-38.000897, -57.541552], 15);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

L.marker([-38.000897, -57.541552]).addTo(map);
L.marker([-38.000076, -57.543273]).addTo(map);
L.marker([-37.997447, -57.543932]).addTo(map);

$.ajax({
    dataType: 'json',
    url: 'api/bicicletas',
    success: function(result) {
        console.log(result);
        result.bicicletas.forEach(element => {
            L.marker(element.ubicacion, { title: element.id }).addTo(map);
        });
    }
})